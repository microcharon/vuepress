# 标题命名格式及标准化

Album: 专辑

Artist(s): 艺术家

Description: 说明

Format: 格式

Language: 语言

Time: 时间

Version: 版本

## 软件类

主名[中文]+主名[英文]+{version}+{description}

## 音乐类

({description})+{artist(s)}+{album}+{time}+{format}

({description})+{artist(s)}+{time}+{"合集"or"discography"}

## 游戏类

主名[中文]+主名[英文]+{version}+{time}+{language}
