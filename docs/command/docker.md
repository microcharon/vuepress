# Docker常用命令

### 查看本地镜像

```bash
docker images
```

### 删除本地镜像

```bash
docker rmi 镜像ID
```

### 查看运行中的容器

```bash
docker ps
```

### 查询所有容器

```bash
docker ps -a
```

### 停止容器

```bash
docker stop 容器ID或name
```

### 删除容器

```bash
docker rm 容器ID或name
```

### 查看容器日志

```bash
docker logs 容器ID或name
```
