# 最常用的主要rclone命令

### rclone about

Get quota information from the remote.

从远端获取配额信息。

### rclone authorize

Remote authorization.

远端认证。

### rclone cat

Concatenate any files and send them to stdout.

连接任意文件并发送到标准输出（stdout）。

### rclone check

Check if the files in the source and destination match.

检查源和目录路径下的文件是否匹配。

### rclone cleanup

Clean up the remote if possible.

尽可能清理远端（回收站）。

### rclone config

Enter an interactive configuration session.

进入交互式配置会话。

### rclone copy

Copy files from source to dest, skipping already copied.

将文件从源复制到目的地，跳过已复制的文件。

### rclone copyto

Copy files from source to dest, skipping already copied.

将文件从源复制到目的地，跳过已经复制的。

### rclone cryptcheck

Check the integrity of a crypted remote.

检查加密远端的完整性。

### rclone dedupe

Interactively find duplicate files and delete/rename them.

交互式查找重复文件并删除/重命名。

### rclone delete

Remove the contents of path.

删除路径的指定的内容。

### rclone genautocomplete

Output shell completion scripts for rclone.

输出rclone的shell完成脚本。

### rclone gendocs

Output markdown docs for rclone to the directory supplied.

将rclone提供的目录输出为markdown文档。

### rclone listremotes

List all the remotes in the config file.

列出配置文件中的所有远端。

### rclone ls

List all the objects in the path with size and path.

用大小和路径列出指定路径下的所有对象。

### rclone lsd

List all directories/containers/buckets in the path.

列出指定目录下的所有目录（directories）/容器（containers）/存储桶（buckets）。

### rclone lsl

List all the objects in the path with size, modification time and path.

以大小、修改时间和路径列出指定路径下的所有对象。

### rclone md5sum

Produce an md5sum file for all the objects in the path.

为指定路径下的所有对象生成一个md5sum文件。

### rclone mkdir

Make the path if it doesn’t already exist.

如果指定路径不存在则创建。

### rclone mount

Mount the remote as a mountpoint.

将远端挂载为一个挂载点。

### rclone move

Move files from source to dest.

将文件从源地址移动到目的地。

### rclone moveto

Move file or directory from source to dest.

将文件或者目录从源移动到目的。

### rclone obscure

Obscure password for use in the rclone.conf

模糊在`rclone.conf`中使用的密码。

### rclone purge

Remove the path and all of its contents.

删除路径已经路径下的所有内容。

### rclone rmdir

Remove the path.

删除指定路径。

### rclone rmdirs

Remove any empty directories under the path.

删除指定路径下的所有空目录。

### rclone sha1sum

Produce a sha1sum file for all the objects in the path.

为指定路径下的所有对象生成一个sha1sum文件。

### rclone size

Return the total size and number of objects in remote:path.

返回`remote:path`路径下对象的总共大小和数量。

### rclone sync

Make source and dest identical, modifying destination only.

使源和目的文件一直，仅修改目的。

### rclone version

Show the version number.

显示rclone版本号。
