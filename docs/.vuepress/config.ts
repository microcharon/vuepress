import { defineUserConfig } from 'vuepress'
const { defaultTheme } = require('@vuepress/theme-default')
const { prismjsPlugin } = require('@vuepress/plugin-prismjs')
const { searchPlugin } = require('@vuepress/plugin-search')

export default defineUserConfig({
  base: '/',
  lang: 'zh-CN',
  title: 'Wikicharon',
  description: 'Test for VuePress',
  colorMode: 'dark',
  head: [['link', { rel: 'icon', type: 'image/svg+xml', href: https://buc.microcharon.top/logo-vuepress.svg' }]],
  plugins: [
    prismjsPlugin({
      preloadLanguages: ['json', 'markdown', 'python', 'yaml']
    }),
    searchPlugin({
      maxSuggestions: 5,
      isSearchable: (page) => page.path !== '/',
    })
  ],
  theme: defaultTheme({
    navbar: [
      // NavbarItem
      {
        text: '基本文档',
        link: '/',
      },
      // NavbarGroup
      {
        text: '技术手册',
        children: ['/tech/name-title.md', '/tech/robots-txt.md']
      },
      {
        text: '命令手册',
        children: ['/command/docker.md', '/command/rclone.md'],
      },
      {
        text: 'GitHub',
        link: 'https://github.com/vercharon',
      },
    ],
    // 侧边栏数组
    // 所有页面会使用相同的侧边栏
    sidebar: [
      // SidebarItem
      {
        text: '基本文档',
        link: '/',
        children: [
          // SidebarItem
          // 字符串 - 页面文件路径
          '/',
        ],
      },
      {
        text: '命令手册',
        link: '/command/docker.md',
        children: [
          '/command/docker.md',
          '/command/rclone.md',
        ],
      },
      {
        text: '技术手册',
        link: '/tech/name-title.md',
        children: [
          '/tech/name-title.md',
          '/tech/robots-txt.md',
        ],
      },
    ],
  }),
})
